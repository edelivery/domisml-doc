
include::_meta/docsconfig.adoc[]
include::_meta/entitites.adoc[]

//
// Visible doc starts here
//

= DomiSML

//Dashboard
ifeval::["{backend}" == "html5"]
include::content/_dashboard/index_dashboard.adoc[]
endif::[]

//includes
include::content/architecture/index_architecture.adoc[]
include::content/quickstart/index_quickstart.adoc[]
include::content/interface/index_icd.adoc[]

== Support

DomiSML Documentation is maintained by the eDelivery Support Team.
For any questions, comments or requests for change, please contact:

* *Email*: ec-edelivery-support@ec.europa.eu
* *Hours*: 8AM to 6PM (Normal EC working days)
